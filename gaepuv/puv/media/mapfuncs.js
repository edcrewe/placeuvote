// Map funcs for dragging around markers

var revgeocoder = new google.maps.Geocoder();
var geocoder = new GClientGeocoder();

function showAddress(map, markers) {
    var address = document.getElementById('id_address_move').value;
    if (address && geocoder) {
	geocoder.getLatLng(
			   address,
			   function(latLng) {
			       if (!latLng) {
				   alert("Sorry, " + address + " not found");
			       } else {
				   var move = document.getElementById('id_select_move').value;
                                   if (move == 'all') {
				       reset_markers(latLng, markers);
				   } else {
				       var latLng = new google.maps.LatLng(latLng.lat(), latLng.lng());
				       markers[move].setPosition(latLng);
				       updateMarkerLocation(latLng, move);
				       geocodePosition(latLng, move);
                                   }   
				   zoom_markers(map, markers, 14);
			       }
			   }
	  );
    }
}

function reset_markers(latLng, markers) {
    var num_marks = markers.length;
    var currentlatLng = new google.maps.LatLng(latLng.lat(), latLng.lng());
    for (i=0;i<num_marks;i++) {
       currentlatLng = new google.maps.LatLng(currentlatLng.lat(), currentlatLng.lng()+0.004*i);
       markers[i].setPosition(currentlatLng);
       updateMarkerLocation(currentlatLng, i);
       geocodePosition(currentlatLng, i); 
    }
 }

function geocodePosition(pos, choice) {
  revgeocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address, choice);
    } else {
      updateMarkerAddress('Cannot determine address at this location.', choice);
    }
  });
}

function updateMarkerPosition(latLng, choice) {
  var info = 'id_info' + choice; 
  var latlngstr = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
  document.getElementById(info).innerHTML = '(' + latlngstr + ')';
}

function updateMarkerLocation(latLng, choice) {
  var id = 'id_f' + choice + '-location';
  var latlngstr = [
    latLng.lat(),
    latLng.lng()
  ].join(', ');
  document.getElementById(id).value = latlngstr
}

function updateMarkerAddress(str, choice) {
  var id = 'id_address' + choice;
  document.getElementById(id).innerHTML = str;
  var id = 'id_f' + choice + '-address';
  document.getElementById(id).value = str;
}

function new_map(map, latLng, num_marks) {
   var latLng = new google.maps.LatLng(latLng.lat(), latLng.lng());
   var markers = new Array();
   for (i=num_marks-1;i>-1;i--) {
       markers[i] = add_drag(map, latLng, i);
   }
   return markers;
}


function add_drag(map, latLng, choice) { 
  if (choice == 20) { 
    return null;
  }
  var latLng = new google.maps.LatLng(latLng.lat(), latLng.lng()+0.004*choice);
  var num = choice + 1;  
  var title = 'Choice ' + num + '.';

  var marker = new google.maps.Marker({
    position: latLng,
    icon: "/media/marker/green" + num + ".png",
    title: title,
    map: map,
    draggable: true});
 
  // updateMarkerPosition(latLng, choice);
  geocodePosition(latLng, choice);

  google.maps.event.addListener(marker, 'dragstart', function() {
    updateMarkerAddress('Dragging...', choice);
  });
 
  //google.maps.event.addListener(marker, 'drag', function() {
  //  updateMarkerPosition(marker.getPosition(), choice);
  //});
 
  google.maps.event.addListener(marker, 'dragend', function() {
    updateMarkerLocation(marker.getPosition(), choice);
    geocodePosition(marker.getPosition(), choice);
  });

  return marker;
}

function zoom_markers(map, markers, zoom) {
    var bounds = new google.maps.LatLngBounds;
    for (var i=0; i<markers.length; i++) {
	bounds.extend(markers[i].getPosition());
    }
    if (markers.length > 1 && !zoom) {
	map.fitBounds(bounds);
    } else {
	if (!zoom) { zoom = 15; }
	map.setCenter(bounds.getCenter());
	map.setZoom(zoom);
    }
}

var location_input = '<span id="id_loc_lineZZ"><img src="/media/marker/greenYY.png" alt="YY. " /><input type="hidden" name="fZZ-location" value="51.500000,-2.600000" id="id_fZZ-location" /><input type="hidden" name="fZZ-address" id="id_fZZ-address" /><input name="fZZ-choice" value="use address below..." maxlength="100" type="text" id="id_fZZ-choice" size="60" /><div class="markerinfo"><span id="id_addressZZ"></span><span id="id_infoZZ"></span></div></span>'; 


function add_location(i) {
    var num = i + 1;
    document.getElementById('id_locations').value = num;
    var html = location_input.replace(new RegExp( "ZZ", "g"), i );
    var html = html.replace(new RegExp( "YY", "g"), num );
    $(html).appendTo('#id_location_inputs');
    $('#id_select_move').append('<option value="'+i+'">-- '+num+' --</option');
    return;
}

function remove_location(i) {
    document.getElementById('id_locations').value = i;
    if(i > 0) { $('#id_loc_line' + i).remove(); }  
    $('#id_select_move option:last').remove();
    return;
}

function add_marker() {
    if (markers.length == 20) { 
      alert('Sorry there is a limit of 20 markers'); 
      return;
    }
    for (var i=0; i<markers.length; i++) {
	if (!markers[i].map) { 
	    markers[i].setMap(map);
	    add_location(i);
            return;
        }
    } 
    add_location(markers.length);
    markers[markers.length] = add_drag(map, map.getCenter(), markers.length);
}

function remove_marker() {
    for (var i=markers.length-1; i>0; i--) {    
        if (markers[i].map != null) {       
	    markers[i].setMap(null);
	    remove_location(i);
            return;
        }
    }
}

/* date picker */

function add_date() {
    $('#id_date_text').datepick({ 
	    multiSelect: 20, 
            monthsToShow: 2, 
            showTrigger: '#calImg'},
	    $.datepick.regional['uk']
);
}


/* validation script - manual validation funcs - saves needing jquery validation plugin */

function initialize_form() {
	var form = $("#id_createpoll_form");
	var question = $("#id_question");
	var questionInfo = $("#id_question_error");
	var created_by = $("#id_created_by");
	var created_byInfo = $("#id_created_by_error");
	var locationsInfo = $("#id_locations_error");
	//	var email = $("#email");
	// var emailInfo = $("#email_error");
	
	//On Submitting
	form.submit(function(){
		if(validate_created_by() && validate_question()) {
                    // Add check of registered user input when that bits built!
		    return confirm("Have you finished creating your poll?\n\nNote: Unregistered users cannot re-edit a poll after its created.\n\nSelect OK to finish or Cancel to continue editing.");
		    //return true;
		} else {
		    return false;
                }
	    });

	//Catch move form submit too so it doesnt submit and bypass other forms validation
	$("#id_move_form").submit(function() {showAddress(map, markers); return false;});
	
    function validate_question(){
		if(question.val().length < 4){
			questionInfo.text("Sorry a question has to have more than 3 letters");
			return false;
		}else{
			questionInfo.text("");
			return true;
		}
    }

    function validate_created_by(){
	    if(created_by.val().length < 1){
		created_byInfo.text("Sorry a name has to be longer than 1 letter") ;
		    //- or use your Google or Facebook account");
		return false;
	    } else{
		return true;
	    }
    }

	
    function validate_email(){
		var a = $("#email").val();
		var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
		if(filter.test(a)){
			emailInfo.text("Valid E-mail please, you will need it to log in");
			return false;
		}else{
			return true;
		}
    }


}
