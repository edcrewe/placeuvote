from django.conf.urls.defaults import *
from puv import settings

urlpatterns = patterns('',
    (r'^$', 'puv.poll.views.index'),
    (r'^atom.xml', 'puv.poll.views.feed', {'template':'atom.xml'}),
    (r'^polls.rss', 'puv.poll.views.feed', {'template':'polls.rss'}),
    (r'^vote/$', 'puv.poll.views.index', {'template':'vote_index.html'}),
    (r'^results/$', 'puv.poll.views.index', {'template':'results_index.html'}),
    (r'^create/$', 'puv.poll.views.create'),
    (r'^poll/(?P<poll_key>[^\.^/]+)/$', 'puv.poll.views.poll_detail'),
    (r'^poll/(?P<poll_key>[^\.^/]+)/results/$', 'puv.poll.views.poll_results'),
    (r'^poll/(?P<poll_key>[^\.^/]+)/results.xml', 'puv.poll.views.poll_results_xml'),
    #(?P<username>[^\.^/]+)
    )

if settings.DEBUG:
    from django.views.static import serve
    _media_url = settings.MEDIA_URL
    if _media_url.startswith('/'):
        _media_url = _media_url[1:]
        urlpatterns += patterns('',
                               (r'^%s(?P<path>.*)$' % _media_url,
                               serve,
                               {'document_root': settings.MEDIA_ROOT}))
        del(_media_url, serve)
