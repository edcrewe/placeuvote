# Django settings for django-puv project.
import os
# sys.path.append('/media/data/gaepuv/placeuvote/puv')

HOST = 'www.placeuvote.com'

if HOST=='localhost':
    DEBUG = True
    TEMPLATE_DEBUG = DEBUG
else:
    DEBUG = True
    TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)


GMAP_KEYS = {'localhost':'ABQIAAAAWVU6ncCdYHlK6BeqLfl1SxT2yXp_ZAY8_ufC3CFXhHIE1NvwkxTzpe852XpRT-kJH-1K61JdUH37vw',
             'www.placeuvote.com':'ABQIAAAAWVU6ncCdYHlK6BeqLfl1SxTL5TU2MnetdI12bh0dAVKKGZ7BThS1ykKnwJCDNe-1PVKY6lr8W1ZB7Q'
            }     
# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/admin/'

    
# Just have same css throughout in base.html 
# for simplicity and performance sake
# PUV_CSS = ['%s/%s' % (MEDIA_URL, 'puv.css'),]
# GMAP_CSS = PUV_CSS.append('%s/%s' % (MEDIA_URL, 'gmap.css'))

MANAGERS = ADMINS

DATABASE_ENGINE = ''           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = ''             # Or path to database file if using sqlite3.
DATABASE_USER = ''             # Not used with sqlite3.
DATABASE_PASSWORD = ''         # Not used with sqlite3.
DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'media')


# Make this unique, and don't share it with anybody.
SECRET_KEY = '#a1g)i%hz+f)44a0wea9ln!g+#=#tke!0@-k)gt=&m#ec-ir=&'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
#     'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
)

ROOT_URLCONF = 'puv.urls'


ROOT_PATH = os.path.dirname(__file__)
TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or
    # "C:/www/django/templates".  Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    ROOT_PATH + '/templates',
)

INSTALLED_APPS = (
    'puv.poll',
)

# TODO - if it gets bigger 
# pass in geocode etc so this works as included js

USE_ADDR = 'use address below...'
DEFAULT_LAT = 51.5
DEFAULT_LNG = -2.6
MAP_INIT = { 'gmap' : 'map_default', 
             'lat' : DEFAULT_LAT, 
             'lng' : DEFAULT_LNG,
             'country' : 'gb'}

GMAP_FUNC_JS = '''
var map = '';
function initialize() {
  // geocoder.setBaseCountryCode('%(country)s')
  var latLng = new google.maps.LatLng(%(lat)s, %(lng)s);
  map = new google.maps.Map(document.getElementById('%(gmap)s'), {
    zoom: 13,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  markers = load_overlay(map, latLng);
}

// Onload handler to fire off the app.
google.maps.event.addDomListener(window, 'load', initialize);
'''

NEW_OVERLAY = '''
function load_overlay(map, latLng){
    initialize_form();
    add_date();
    return new_map(map, latLng, 3);
}
'''

GMAP_FUNC = GMAP_FUNC_JS % MAP_INIT

GMAP_JS = ['<script src="%s%s" type="text/javascript"></script>' %  
             ("http://maps.google.com/maps?file=api&amp;v=3&amp;key=",
               GMAP_KEYS[HOST]),
          '<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>',  
          '<script src="/media/mapfuncs.js" type="text/javascript"></script>',
          '<script type="text/javascript">%s</script>' % GMAP_FUNC,
          '<script type="text/javascript">%s</script>' % NEW_OVERLAY
          ]

JQUERY = '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" type="text/javascript"></script>' 

