from django import forms
import models
from google.appengine.ext.db import djangoforms
from puv.poll.widgets import LocationFormField
from puv.settings import GMAP_JS, USE_ADDR
from puv.poll.utils import grab_urls

class PollForm(djangoforms.ModelForm):
    created_by = forms.CharField(max_length = 100, required=True,
                                 widget=forms.widgets.TextInput(attrs={'size':53}))
    email = forms.CharField(max_length = 100, required=False,
                            widget=forms.widgets.TextInput(attrs={'size':58}))
    question = forms.CharField(max_length = 100, required=True,
                     widget=forms.widgets.Textarea(attrs={'rows':2, 
                                                          'cols':60}))    
    locations = forms.IntegerField(widget=forms.widgets.HiddenInput(), required=False)

    class Meta:
        model = models.Poll
#        exclude = ['created_by']
 
    def _generate_js(self):
        """ Generate javascript based on locations in poll """
        return GMAP_JS

    genjs = property(_generate_js)


class ChoiceForm(forms.Form):
    location = LocationFormField()
    choice = forms.CharField(max_length = 100,
                             widget=forms.widgets.TextInput(attrs={'size':60}))
    address = forms.CharField(widget=forms.widgets.HiddenInput())

    class Meta:
        ordering = ['votes']

    def __init__(self, poll=None, *args, **kwargs):
        self.poll = poll
        super(ChoiceForm, self).__init__(*args, **kwargs)
        
    def save(self):
        cleaned = super(ChoiceForm, self).clean()
        choice = cleaned.get('choice','')
        url = None
        if choice == USE_ADDR:
            choice = ''
        else:
            urls = grab_urls(choice)
            if urls:
                for url in urls:
                    choice = choice.replace(url,'')
                url = urls[0]

        entry = models.Choice(poll = self.poll, 
                              choice = choice,
                              location = cleaned.get('location',''),
                              address = cleaned.get('address',''),
                              url = url)
        entry.put()
        

