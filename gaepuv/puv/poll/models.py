from google.appengine.ext import db
from django import forms
from django.utils.safestring import mark_safe

class Poll(db.Model):
    """ Top level poll 
        voter flags record voter info
        locations and dates are the number of those types of choices
        GAE doesnt do aggregate funcs - so record them here (last and total votes) 
    """
    question = db.StringProperty()
    private = db.BooleanProperty(default = False)
    anonymous = db.BooleanProperty(default = False)
    onechoice = db.BooleanProperty(default = False)
    locations = db.IntegerProperty(default = 3)
    dates = db.IntegerProperty(default = 0)
    created_on = db.DateTimeProperty(auto_now_add = 1)
    created_by = db.StringProperty()
    last_vote = db.DateTimeProperty(auto_now = 1)
    total_votes = db.IntegerProperty(default = 0)
    user = db.UserProperty()
    email = db.EmailProperty()
    
    def __str__(self):
        return '%s' % self.question
    
    def get_absolute_url(self):
        return mark_safe('/poll/%s/' % self.key())
    
class Choice(db.Model):
    """ Location choices """
    poll = db.ReferenceProperty(Poll)
    choice = db.StringProperty()
    location = db.GeoPtProperty()
    address = db.PostalAddressProperty()
    url = db.LinkProperty()
    votes = db.IntegerProperty(default = 0)

    class Meta:
        ordering = ('-votes',)

    def __str__(self):
        html = ''
        if self.url:
            html = '<a href="%s">' % self.url
        if self.choice:
            html += self.choice
        else: 
            html += self.address
        if self.url:
            html += '</a>'
        return mark_safe(html) 

class DateChoice(db.Model):
    """ Date and/or Time choices - dateortime is
        0 = date, 1 = time, 2 = date and time
    """
    poll = db.ReferenceProperty(Poll)    
    datetime = db.DateTimeProperty()
    dateortime = db.IntegerProperty(default = 0)
    votes = db.IntegerProperty(default = 0)

    class Meta:
        ordering = ('datetime',)

class Voter(db.Model):
    """ Record voter data for polls with anon = False """
    poll = db.ReferenceProperty(Poll)    
    choice = db.ReferenceProperty(Choice)
    datechoice = db.ReferenceProperty(DateChoice)
    created_by = db.StringProperty()
    created_on = db.DateTimeProperty(auto_now_add = 1)
    user = db.UserProperty()

class Country(db.Model):
    """ 2 letter iso code to geocode """
    iso = db.StringProperty()
    location = db.GeoPtProperty()
