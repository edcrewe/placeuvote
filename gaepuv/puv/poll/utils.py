# Utility funcs 
import re
from google.appengine.api import memcache
from puv.poll.models import Country
from google.appengine.api import mail

def mail_poll(poll):
    """ Email users with poll url who have requested it """
    if not mail.is_email_valid(poll.email):
       return False
    message = mail.EmailMessage(sender="placeUvote.com Support <edmundcrewe@gmail.com>",
                                subject="placeUvote poll created")
    message.to = "%s <%s>" % (poll.created_by, poll.email)
    emaildata = (poll.created_by, poll.get_absolute_url(), poll.question)
    message.body = """
      Dear %s:

      Your placeUvote.com poll has been created, to vote in the poll please visit:

      http://www.placeuvote.com%s
      
      To answer the question:

      %s

      Please let us know if you have any issues.

      The placeUvote.com Team
      """ % emaildata
    message.send()
    return True

def getGeoIPCode(ipaddr):
   """ IP based country lookup and convert to latLng """
   memcache_key = "ip_%s" % ipaddr
   data = memcache.get(memcache_key)
   if data is not None:
      return data
   if ipaddr == '127.0.0.1':
       iso = 'gb'
   else:
       iso = ''
   # debug ipaddr = '173.194.37.104'
   if not iso:
       from google.appengine.api import urlfetch
       try:
          fetch_response = urlfetch.fetch('http://geoip.wtanaka.com/cc/%s' % ipaddr)
          if fetch_response.status_code == 200:
             iso = fetch_response.content
       except urlfetch.Error, e:
          pass

   if iso:
      if iso == 'zz':
         iso = 'gb'
      country = Country.all().filter('iso =', iso.upper())
      if country.count():      
         memcache.set(memcache_key, [iso, country[0].location])
         return iso, country[0].location
   return 'gb', '' 

urls = '(?: %s)' % '|'.join("""http telnet gopher file wais ftp""".split())
ltrs = r'\w'
gunk = r'/#~:.?+=&%@!\-'
punc = r'.:?\-'
any = "%(ltrs)s%(gunk)s%(punc)s" % { 'ltrs' : ltrs,
                                     'gunk' : gunk,
                                     'punc' : punc }
pat = r""" \b %(urls)s : [%(any)s]  +? (?=[%(punc)s]* (?:[^%(any)s] | $ ) )
       """ % {'urls' : urls,
              'any' : any,
              'punc' : punc }
url_re = re.compile(pat, re.VERBOSE | re.MULTILINE)

def grab_urls(text):
    """Given a text string, returns all the urls we can find in it."""
    return url_re.findall(text)

