from google.appengine.ext import db
from google.appengine.tools import bulkloader

class Country(db.Model):
    """ 2 letter iso code to geocode """
    iso = db.StringProperty()
    location = db.GeoPtProperty()


class CountryLoader(bulkloader.Loader):
    """ see http://code.google.com/appengine/docs/python/tools/uploadingdata.html 
        Note: dont have any header column labels and ensure all data is correct format
        Uploader will automatically convert strings to a float list for  geo points
    """
    def __init__(self):
        bulkloader.Loader.__init__(self, 'Country',
                                   [('iso', str),
                                    ('location', str)
                                   ])
loaders = [CountryLoader]
