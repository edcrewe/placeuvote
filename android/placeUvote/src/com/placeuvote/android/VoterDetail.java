package com.placeuvote.android;

import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.placeuvote.android.R;
import com.placeuvote.android.PUVDatabase.LocationsCursor;
import com.placeuvote.android.PUVDatabase.VoterCursor;


/**
 * VoterDetail
 */
public class VoterDetail extends Activity {
    private static Integer voter_id;
    private static TextView txtVoter;
    private static TextView txtKey;
    private static Integer poll_id;
    private static Integer choice_id;
    private static Integer datechoice_id;
    private static TextView CreatedOn;

    private VoterCursor voter;
    
     
    /**
     * Called when the activity is first created.
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voterdetail);
   /**     voter_id = (Integer) findViewById(R.id._id);
        choice_id = (Integer) findViewById(R.id.choice_id);
        datechoice_id = (Integer) findViewById(R.id.datechoice_id);
   */
        txtVoter = (TextView) findViewById(R.id.txtVoter);
        txtKey = (TextView) findViewById(R.id.txtKey);
        CreatedOn = (TextView) findViewById(R.id.dateCreatedOn);

        // get the _id for this voter from the bundle passed by VoterDetails
        Bundle b = this.getIntent().getExtras();
        voter_id = Integer.valueOf(b.getInt("_id"));
        PUVDatabase db = new PUVDatabase(this);
        voter = db.getVoter(voter_id.longValue());

        // fill in the form and display
        txtVoter.setText(voter.getColCreatedBy());
        txtKey.setText(voter.getColVoterKey());
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
        CreatedOn.setText("Created: " + dateFormat.format(voter.getColCreatedOn()));
     }

    /**
     * @see android.app.Activity#onResume()
     */
    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * @param icicle
     */
    public void onPause(Bundle icicle) {
        super.onPause();
    }

    /**
     * Make sure to stop the animation when we're no longer on screen, failing
     * to do so will cause a lot of unnecessary cpu-usage!
     */
    @Override
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }

    /**
     * Setup menus for this page.
     * 
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean supRetVal = super.onCreateOptionsMenu(menu);
        menu.add(0, 0, Menu.NONE, getString(R.string.poll_detail_menu_back_to_location_info));
        return supRetVal;
    }

    /**
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                // Go back to the list page
                finish();
                return true;
            case 1:
                // Go to the voter detail page
                Intent i = new Intent(VoterDetail.this, PollVote.class);
                Bundle b = new Bundle();
                b.putInt("_id", voter_id.intValue());
                i.putExtras(b);

                startActivity(i);
                return true;
            default:
                return false;
        }
    }
}
