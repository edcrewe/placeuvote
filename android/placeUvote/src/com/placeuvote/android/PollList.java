package com.placeuvote.android;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableLayout.LayoutParams;

import com.placeuvote.android.R;
import com.placeuvote.android.PUVDatabase.LocationsCursor;
import com.placeuvote.android.PUVDatabase.PollsCursor;
/**
 * PollsList
 */
public class PollList extends Activity {
	
    private static class puvButton extends Button {
        protected int jrow; // puvButton is just a button that knows which poll number it's associated with

        public puvButton(Context btnContext) {
            super(btnContext);
        }
    }

    private static Button btnQuestion;
    private static Button btnView;
    private static Button btnVote;
    private static Button btnCreate;
    private static Spinner spnPoll;
    static TableLayout tblPolls;

    

    // Create a button click listener for the Question buttons in the list
    // Clicking on any of these should take us to a detail listing for that
    // job
    private final Button.OnClickListener btnViewClick = new Button.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(PollList.this, PollVote.class);
            Bundle b = new Bundle();
            puvButton vb = (puvButton) v;
            cursor.moveToPosition(vb.jrow);
            b.putInt("_id", (int) cursor.getColPollId());
            i.putExtras(b);

            startActivity(i);
        }
    };

    private PollsCursor cursor;
    private PUVDatabase db;

    ArrayList<View> lstTable;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.polllist);

        btnView = (Button) findViewById(R.id.btnView);
        btnView.setOnClickListener(btnViewClick);
        spnPoll = (Spinner) findViewById(R.id.spnPolls);
        spnPoll.setTag("Poll");
        
        db = new PUVDatabase(this);
        
        fillData(PollsCursor.SortBy.CreatedOn);
    }

    /**
     * @see android.app.Activity#onResume()
     */
    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * @param icicle
     */
    public void onPause(Bundle icicle) {
        super.onPause();
    }

    /**
     * Make sure to stop the animation when we're no longer on screen, failing 	public int getColVoterId(){return getInt(getColumnIndexOrThrow("_id"));}
    	public String getColVoterKey(){return getString(getColumnIndexOrThrow("key"));}
    	public int getColPollsId(){return getInt(getColumnIndexOrThrow("poll_id"));}
    	public int getColLoctionsId(){return getInt(getColumnIndexOrThrow("choice_id"));}
    	public int getColDatesId(){return getInt(getColumnIndexOrThrow("datechoice_id"));}
     	public String getColCreatedBy(){return getString(getColumnIndexOrThrow("createdby"));}
     	public long getColCreattedOn(){return getLong(getColumnIndexOrThrow("createdon"));}
     * to do so will cause a lot of unnecessary cpu-usage!
     */
    @Override
    public void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
    }

    /**
     * Setup menus for this page
     * 
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean supRetVal = super.onCreateOptionsMenu(menu);
        menu.add(0, 0, Menu.NONE, getString(R.string.list_menu_back_to_map));
        menu .add(0, 1, Menu.NONE, getString(R.string.list_menu_sort_by_poll));
        menu.add(0, 2, Menu.NONE, getString(R.string.list_menu_add_dates));
        menu.add(0, 3, Menu.NONE, getString(R.string.list_menu_add_location));
        return supRetVal;
    }


    /**Poll
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                // Go back to the map page
                finish();
                return true;
            case 1:
                // Sort the list by poll Question
                for (View vw : lstTable) {
                    tblPolls.removeView(vw);
                }
                fillData(PollsCursor.SortBy.Question);
                return true;
            case 2:
                // Sort the list by poll creation date
                for (View vw : lstTable) {
                    tblPolls.removeView(vw);
                }
                fillData(PollsCursor.SortBy.CreatedOn);
                return true;
            case 3:
            	// Add a new location
                Intent i = new Intent(PollList.this, AddPoll.class);
                startActivity(i);
                 	return true;
        }

        return false;
    }

    /**
     * @param sortBy
     */
    void fillData(PollsCursor.SortBy sortBy) {
        // Create a new list to track the addition of TextViews
        lstTable = new ArrayList<View>(); // a list of the TableRow's added
        // Get all of the rows from the database and create the table
        // Keep track of the TextViews added in list lstTable
        cursor = db.getPolls(sortBy);
        // Create a table row that contains two lists
        // (one for job Questions, one for employers)
        // Now load the lists with job Question and employer name
        //for (Locations row : rows) {
        for( int rowNum=0; rowNum<cursor.getCount(); rowNum++){
        	cursor.moveToPosition(rowNum);
            TableRow tr = new TableRow(this);
            tr.setLayoutParams(new LayoutParams(
                android.view.ViewGroup.LayoutParams.FILL_PARENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
            // Create a Button for the job Question.
            puvButton btn1 = new puvButton(this);
            // Button btn1 = new Button(this);
            btn1.jrow = rowNum;
            btn1.setText(cursor.getColQuestion());
            btn1.setPadding(1, 0, 3, 0);
            btn1.setHeight(40);
            btn1.setGravity(android.view.Gravity.CENTER);
            // btn1.setBackgroundColor(colorByStatus((int) cursor.getColStatus()));
            btn1.setOnClickListener(btnViewClick);
            // Add poll Question to poll list.
            tr.addView(btn1);

            Button btn2 = new Button(this);
            btn2.setPadding(1, 0, 3, 0);
            btn2.setText(cursor.getColQuestion());
            btn2.setHeight(40);
            btn2.setGravity(android.view.Gravity.CENTER);
            btn2.setBackgroundColor(Color.WHITE);
            /* Add employer name to that list. */
            tr.addView(btn2);

            tblPolls.addView(tr);
            // lstLocations.add(btn1.getId()); // keep job id to get more info later if needed
            lstTable.add(tr); // keep track of the rows we've added (to remove later)
        }
    }

    // Set a background color based on the current status of a job
    private int colorByStatus(int status) {
        switch (status) {
            case 1: // Position is taken
                return Color.argb(150, 255, 0, 0); // red
            case 2: // There are applicants for the position
                return Color.argb(150, 44, 211, 207); // yellow
            case 3: // The position is available
                return Color.argb(150, 0, 255, 0); // green
            default:
                return Color.WHITE;
        }
    }
}
