package com.placeuvote.android;

import com.placeuvote.android.R;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteCursorDriver;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQuery;
import android.util.Log;

/**
 * Provides access to the placeUvote locally cached database. 
 * Note there is a separate synching process to push this data via RSS to and from placeuvote.com
 * Since this is not a Content Provider, no other applications will have access to the database.
 */
public class PUVDatabase extends SQLiteOpenHelper {
	/** The name of the database file on the file system */
    private static final String DATABASE_NAME = "placeuvote.db";
    /** The version of the database that this class understands. */
    private static final int DATABASE_VERSION = 1;
    /** Keep track of context so that we can load SQL from string resources */
    private final Context mContext;

    /**
     * Provides self-contained query-specific cursor for Polls.  
     * The query and all accessor methods are in the class.
     */
    public static class PollsCursor extends SQLiteCursor{
    	/** The query for this cursor */
    	public static enum SortBy{
    		Question,
    		CreatedOn
    	}
    	private static final String QUERY = 
    		"SELECT id, key, question, private, "+
    		"anonymous, onechoice, locations, dates, "+
    		"createdon, createdby, last_vote, total_votes, user, email " +
       		"FROM poll where id = "; 
       		//"ORDER BY ";  		
    		//where _id =";
    	/** Cursor constructor */
		private PollsCursor(SQLiteDatabase db, SQLiteCursorDriver driver,
				String editTable, SQLiteQuery query) {
			super(db, driver, editTable, query);
		}
		/** Private factory class necessary for rawQueryWithFactory() call */
	    private static class Factory implements SQLiteDatabase.CursorFactory{
			public Cursor newCursor(SQLiteDatabase db,
					SQLiteCursorDriver driver, String editTable,
					SQLiteQuery query) {
				return new PollsCursor(db, driver, editTable, query);
			}
	    }
	    /* Accessor functions -- one per database column */
    	public int getColPollId(){return getInt(getColumnIndexOrThrow("_id"));}
       	public String getColKey(){return getString(getColumnIndexOrThrow("key"));}
      	public int getColPrivate(){return getInt(getColumnIndexOrThrow("private"));}
      	public int getColAnonymous(){return getInt(getColumnIndexOrThrow("anonymous"));}
      	public int getColOneChoice(){return getInt(getColumnIndexOrThrow("onechoice"));}
      	public int getColLocations(){return getInt(getColumnIndexOrThrow("locations"));}
      	public long getColDates(){return getLong(getColumnIndexOrThrow("dates"));}
     	public long getColCreatedOn(){return getLong(getColumnIndexOrThrow("createdon"));}
     	public String getColCreatedBy(){return getString(getColumnIndexOrThrow("createdby"));}
     	public long getColLastVote(){return getLong(getColumnIndexOrThrow("last_vote"));}
     	public int getColTotalVotes(){return getInt(getColumnIndexOrThrow("total_votes"));}
     	public String getColUser(){return getString(getColumnIndexOrThrow("user"));}
     	public String getColEmail(){return getString(getColumnIndexOrThrow("email"));}	
    	public String getColQuestion(){return getString(getColumnIndexOrThrow("question"));}
    }

    /**
     * Provides self-contained query-specific cursor for Location Detail.  
     * The query and all accessor methods are in the class.
     */
    public static class LocationsCursor extends SQLiteCursor{
    	/** The query for this cursor */
    	private static final String QUERY = 
	    "select id, key, poll_id, choice, loc_lat, loc_long, "+
        "address, url, votes from choice where poll_id = ";   	
    	/** Cursor constructor */
		private LocationsCursor(SQLiteDatabase db, SQLiteCursorDriver driver,
				String editTable, SQLiteQuery query) {
			super(db, driver, editTable, query);
		}
		/** Private factory class necessary for rawQueryWithFactory() call */
	    private static class Factory implements SQLiteDatabase.CursorFactory{
			public Cursor newCursor(SQLiteDatabase db,
					SQLiteCursorDriver driver, String editTable,
					SQLiteQuery query) {
				return new LocationsCursor(db, driver, editTable, query);
			}
	    }
	    /* Accessor functions -- one per database column */
    	public int getColLocationsId(){return getInt(getColumnIndexOrThrow("_id"));}
    	public String getColLocationsKey(){return getString(getColumnIndexOrThrow("key"));}
    	public int getColPollId(){return getInt(getColumnIndexOrThrow("poll_id"));}
    	public String getColChoice(){return getString(getColumnIndexOrThrow("choice"));}
    	public long getColLat(){return getLong(getColumnIndexOrThrow("loc_lat"));}
    	public long getColLong(){return getLong(getColumnIndexOrThrow("loc_long"));}
    	public String getColAddress(){return getString(getColumnIndexOrThrow("address"));}
    	public String getColURL(){return getString(getColumnIndexOrThrow("url"));}
    	public int getColVotes(){return getInt(getColumnIndexOrThrow("votes"));}
    }
	
    public static class DatesCursor extends SQLiteCursor{
    	private static final String QUERY = 
  	    "SELECT id, key, poll_id, datetime, dateortime, votes "+
	    "where poll_id = ";
	    private DatesCursor(SQLiteDatabase db, SQLiteCursorDriver driver,
				String editTable, SQLiteQuery query) {
			super(db, driver, editTable, query);
		}
	    private static class Factory implements SQLiteDatabase.CursorFactory{
		
			public Cursor newCursor(SQLiteDatabase db,
					SQLiteCursorDriver driver, String editTable,
					SQLiteQuery query) {
				return new DatesCursor(db, driver, editTable, query);
			}
	    }
    	public int getColDatesId(){return getInt(getColumnIndexOrThrow("_id"));}
    	public String getColDatesKey(){return getString(getColumnIndexOrThrow("key"));}
    	public int getColPollId(){return getInt(getColumnIndexOrThrow("poll_id"));}
    	public long getColDateTime(){return getLong(getColumnIndexOrThrow("datetime"));}
    	public long getColDateOrTime(){return getLong(getColumnIndexOrThrow("dateortime"));}
    	public long getColVotes(){return getLong(getColumnIndexOrThrow("votes"));}
    }

    /**
     * Provides self-contained query-specific cursor for Polls info.  
     * The query and all accessor methods are in the class.
     * Note: for now there is only one record in this table, so this is a lot of 
     * work to store/retrieve that data.  We do it this way in anticipation of a
     * day when there would be more than one Polls in the table.
     */
    public static class VoterCursor extends SQLiteCursor{
    	/** The query for this cursor */
    	private static final String QUERY = 
    		"SELECT id, key, poll_id, choice_id, datechoice_id,"+ 
    		"created_by, created_on"+
    		"FROM voter ";
    	/** Cursor constructor */
		private VoterCursor(SQLiteDatabase db, SQLiteCursorDriver driver,
				String editTable, SQLiteQuery query) {
			super(db, driver, editTable, query);
		}
		/** Private factory class necessary for rawQueryWithFactory() call */
	    private static class Factory implements SQLiteDatabase.CursorFactory{
		
			public Cursor newCursor(SQLiteDatabase db,
					SQLiteCursorDriver driver, String editTable,
					SQLiteQuery query) {
				return new VoterCursor(db, driver, editTable, query);
			}
	    }
	    /* Accessor functions -- one per database column */
    	public int getColVoterId(){return getInt(getColumnIndexOrThrow("_id"));}
    	public String getColVoterKey(){return getString(getColumnIndexOrThrow("key"));}
    	public int getColPollId(){return getInt(getColumnIndexOrThrow("poll_id"));}
    	public int getColLoctionsId(){return getInt(getColumnIndexOrThrow("choice_id"));}
    	public int getColDatesId(){return getInt(getColumnIndexOrThrow("datechoice_id"));}
     	public String getColCreatedBy(){return getString(getColumnIndexOrThrow("createdby"));}
     	public long getColCreatedOn(){return getLong(getColumnIndexOrThrow("createdon"));}
    }
	
    /** Constructor */
    public PUVDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
	}

    /**
     * Execute all of the SQL statements in the String[] array
     * @param db The database on which to execute the statements
     * @param sql An array of SQL statements to execute
     */
    private void execMultipleSQL(SQLiteDatabase db, String[] sql){
    	for( String s : sql )
    		if (s.trim().length()>0)
    			db.execSQL(s);
    }
    
    /** Called when it is time to create the database */
	@Override
	public void onCreate(SQLiteDatabase db) {
		String[] sql = mContext.getString(R.string.PUVDatabase_onCreate).split("\n");
		db.beginTransaction();
		try {
			// Create tables & test data
			execMultipleSQL(db, sql);
			db.setTransactionSuccessful();
		} catch (SQLException e) {
            Log.e("Error creating tables and debug data", e.toString());
        } finally {
        	Log.e("Created database tables", "OK");
        	db.endTransaction();
        }
	}
	
	/** Called when the database must be upgraded */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(PollMap.LOG_TAG, "Upgrading database from version " + oldVersion + " to " +
                newVersion + ", which will destroy all old data");

		String[] sql = mContext.getString(R.string.PUVDatabase_onUpgrade).split("\n");
		db.beginTransaction();
		try {
			// Create tables & test data
			execMultipleSQL(db, sql);
			db.setTransactionSuccessful();
		} catch (SQLException e) {
            Log.e("Error creating tables and debug data", e.toString());
        } finally {
        	db.endTransaction();
        }

        // This is cheating.  In the real world, you'll need to add columns, not rebuild from scratch
        onCreate(db);
	}

	/**
	 * Add a new poll to the database.  The poll will have a status of open.
	 * @param job_id	The employer offering the poll
	 * @param question			The poll question
	 * @param email	The poll email
	 */
	public void addPoll(long poll_id, String question, String email){
		ContentValues map = new ContentValues();
		map.put("question", question);
		map.put("email", email);
		try{
			getWritableDatabase().insert("polls", null, map);
		} catch (SQLException e) {
            Log.e("Error writing new poll", e.toString());
		}
	}

	/**
	 * Update a poll in the database.
	 * @param poll_id	The poll id of the existing poll
	 * @param question	The poll question
	 * @param email	The poll creators email
	 */
	public void editPoll(long poll_id, String question, String email) {
		ContentValues map = new ContentValues();
		map.put("question", question);
		map.put("email", email);
		String[] whereArgs = new String[]{Long.toString(poll_id)};
		try{
			getWritableDatabase().update("polls", map, "_id=?", whereArgs);
		} catch (SQLException e) {
            Log.e("Error updating existing poll", e.toString());
		}
	}

	/**
	 * Delete a location from the database.
	 * @param location_id		The location_id of the choice to delete
	 */
	public void deleteLocation(long location_id) {
		String[] whereArgs = new String[]{Long.toString(location_id)};
		try{
			getWritableDatabase().delete("choice", "_id=?", whereArgs);
		} catch (SQLException e) {
            Log.e("Error deleteing location", e.toString());
		}
	}

	/** Returns the number of Locations */
	public int getLocationsCount(long pollId){

		Cursor c = null;
        try {
            c = getReadableDatabase().rawQuery("SELECT count(*) FROM choice where poll_id =" + pollId, null);
            if (0 >= c.getCount()) { return 0; }
            c.moveToFirst();
            return c.getInt(0);
        }
        finally {
            if (null != c) {
                try { c.close(); }
                catch (SQLException e) { }
            }
        }
	}

	/** Returns a PollsCursor for a Poll
     */
    public PollsCursor getPollDetails(long pollId) {
    	SQLiteDatabase d = getReadableDatabase();
    	PollsCursor c = (PollsCursor) d.rawQueryWithFactory(
			new PollsCursor.Factory(),
	    	PollsCursor.QUERY + pollId,
			null,
			null);
    	c.moveToFirst();
        return c;
    }
	
    /** Returns a PollsCursor for all Polls
     */
    public PollsCursor getPolls(PollsCursor.SortBy sortBy) {
    	SQLiteDatabase d = getReadableDatabase();
    	PollsCursor c = (PollsCursor) d.rawQueryWithFactory(
			new PollsCursor.Factory(),
	    	PollsCursor.QUERY + sortBy.toString(),
			null,
			null);
    	c.moveToFirst();
        return c;
    }

    /** Returns a sorted DatesCursor for the specified pollId
     * @param pollId The _id of the poll
     */
    public DatesCursor getDates(long pollId) {
    	String sql = DatesCursor.QUERY + pollId + " order by datetime";
    	SQLiteDatabase d = getReadableDatabase();
    	DatesCursor c = (DatesCursor) d.rawQueryWithFactory(
			new DatesCursor.Factory(),
			sql,
			null,
			null);
    	c.moveToFirst();
        return c;
    }

    /** Return a LocationsCursor
     * @param sortBy the sort criteria
     */
    public LocationsCursor getLocations(long pollId) {
    	String sql = LocationsCursor.QUERY + pollId;
    	SQLiteDatabase d = getReadableDatabase();
        LocationsCursor c = (LocationsCursor) d.rawQueryWithFactory(
        	new LocationsCursor.Factory(),
        	sql,
        	null,
        	null);
        c.moveToFirst();
        return c;
    }
    /** Returns the VoterCursor
     * 
     */
    public VoterCursor getVoter(long pollId) {
    	String sql = VoterCursor.QUERY + pollId;
    	SQLiteDatabase d = getReadableDatabase();
    	VoterCursor c = (VoterCursor) d.rawQueryWithFactory(
			new VoterCursor.Factory(),
			sql,
			null,
			null);
    	c.moveToFirst();
        return c;
    }

}
