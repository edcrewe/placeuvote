package com.placeuvote.android;	

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.placeuvote.android.R;
import com.placeuvote.android.PUVDatabase.LocationsCursor;
import com.placeuvote.android.PUVDatabase.PollsCursor;


/**
 * EditLocation
 */
public class EditPoll extends ListActivity {
    private static Spinner spnPoll;
    private static TextView txtQuestion;
    private static TextView txtEmail;
    private static Integer poll_id;
    private static Button btnUpdate, btnCancel;
    private PUVDatabase db;
    private PollsCursor poll;
    private LocationsCursor locations;
    
    private class Poll {
    	public String Question;
    	public long id;
    	Poll( long id, String Question){
    		this.id = id;
    		this.Question = Question;
    	}
    	@Override
    	public String toString() {
    		return this.Question;
    	}
    }

    
    // Create a button click listener for the Update button.
    private final Button.OnClickListener btnUpdateOnClick = new Button.OnClickListener() {
        public void onClick(View v) {
        	Poll poll = (Poll)spnPoll.getSelectedItem();
//        	Toast.makeText(
//        			EditLocation.this, 
//        			String.format(
//        					"Location: %d\nPoll: %s (%d)\nTitle: %s\nDesc: %s", 
//        					poll_id,
//        					employer.Question, 
//        					employer.id,
//        					txtQuestion.getText(),
//        					txtEmail.getText()
//        			), 
//        			Toast.LENGTH_SHORT
//        	).show();
        	if (txtQuestion.getText().length()==0 || txtEmail.getText().length()==0){
	        	Toast.makeText(EditPoll.this, "Fill out the form completely first.", Toast.LENGTH_LONG).show();
        	} else {
	        	db.editPoll((long)poll_id, txtQuestion.getText().toString(), txtEmail.getText().toString());
	        	Toast.makeText(EditPoll.this, "Location updated", Toast.LENGTH_SHORT).show();
	        	finish();
        	}
        }
    };

    // Create a button click listener for the Cancel button.
    private final Button.OnClickListener btnCancelOnClick = new Button.OnClickListener() {
        public void onClick(View v) {
        		finish();
        	}
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editpoll);
        txtQuestion = (TextView) findViewById(R.id.txtQuestion);
        spnPoll = (Spinner) findViewById(R.id.spnPoll);

        // get the poll_id for this poll from the bundle passed by MicroLocationsDetail
        Bundle bIn = this.getIntent().getExtras();
        poll_id = Integer.valueOf(bIn.getInt("_id"));

        db = new PUVDatabase(this);
        poll = db.getPollDetails(poll_id.longValue());
        
        txtQuestion.setText(poll.getColQuestion());

        btnUpdate = (Button) findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(btnUpdateOnClick);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(btnCancelOnClick);

        // spnPoll
        List<Poll> pollsList = new ArrayList<Poll>();
        PollsCursor c = db.getPolls(null);
        int position=0;
        for(int i=0; i<c.getCount(); i++){
        	c.moveToPosition(i);
        	pollsList.add(new Poll(c.getColPollId(),c.getColQuestion()));
        	if (c.getColPollId()==poll.getColPollId())
        		position=i;
        }
        
        ArrayAdapter<Poll> aspnPolls = new ArrayAdapter<Poll>(
        		this, android.R.layout.simple_spinner_item, pollsList);
        aspnPolls.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnPoll.setAdapter(aspnPolls);
        spnPoll.setSelection(position);
    }
}
