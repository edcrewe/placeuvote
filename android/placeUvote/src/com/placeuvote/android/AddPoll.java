package com.placeuvote.android;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.placeuvote.android.R;
import com.placeuvote.android.PUVDatabase.PollsCursor;


/**
 * AddLocation
 */
public class AddPoll extends Activity {
    private static Button btnAddLocation;
    private static Spinner spnPoll;
    private static TextView txtQuestion;
    private static TextView txtEmail;
    private PUVDatabase db;

    private class Poll {
    	public String PollName;
    	public long id;
    	Poll( long id, String PollName){
    		this.id = id;
    		this.PollName = PollName;
    	}
    	@Override
    	public String toString() {
    		return this.PollName;
    	}
    }

    // Create a button click listener for the AddLocation button.
    private final Button.OnClickListener btnAddLocationOnClick = new Button.OnClickListener() {
        public void onClick(View v) {
        	Poll Poll = (Poll)spnPoll.getSelectedItem();
//        	Toast.makeText(
//    			AddLocation.this, 
//    			String.format(
//					"Poll: %s (%d)\nTitle: %s\nDesc: %s", 
//					Poll.PollName, 
//					Poll.id,
//					txtTitle.getText(),
//					txtDescription.getText()
//				), 
//    			Toast.LENGTH_SHORT
//			).show();
        	if (Poll.id<0 || txtQuestion.getText().length()==0 || txtEmail.getText().length()==0){
	        	Toast.makeText(AddPoll.this, "Fill out the form completely first.", Toast.LENGTH_LONG).show();
        	} else {
	        	db.addPoll(Poll.id, txtQuestion.getText().toString(), txtEmail.getText().toString());
	        	Toast.makeText(AddPoll.this, "Location added", Toast.LENGTH_SHORT).show();
	        	//spnPoll.setSelection(0); // select "choose an Poll"
	        	txtQuestion.setText("");
	        	txtEmail.setText("");
        	}
    	}
    };


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new PUVDatabase(this);
        setContentView(R.layout.addpoll);

        // Find the controls
        btnAddLocation = (Button) findViewById(R.id.btnAddLocation);
        spnPoll = (Spinner) findViewById(R.id.spnPoll);
        txtQuestion = (TextView) findViewById(R.id.txtQuestion);
        txtEmail = (TextView) findViewById(R.id.txtEmail);

        // btnAddLocation
        btnAddLocation.setOnClickListener(btnAddLocationOnClick);

        // spnPoll
        List<Poll> PollsList = new ArrayList<Poll>();
        PollsList.add(new Poll(-1, "Choose an Poll"));
        PollsCursor c = db.getPolls(null);
        for(int i=0; i<c.getCount(); i++){
        	c.moveToPosition(i);
        	PollsList.add(new Poll(c.getColPollId(),c.getColQuestion()));
        }
        
        ArrayAdapter<Poll> aspnPolls = new ArrayAdapter<Poll>(
        		this, android.R.layout.simple_spinner_item, PollsList);
        aspnPolls.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnPoll.setAdapter(aspnPolls);
    }

    /**
     * @see android.app.Activity#onResume()
     */
    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * @param icicle
     */
    public void onPause(Bundle icicle) {
        super.onPause();
    }

}
