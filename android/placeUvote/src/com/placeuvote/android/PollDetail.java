package com.placeuvote.android;

import com.placeuvote.android.R;


import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;
import android.view.*;

import com.placeuvote.android.PollMap;
import com.placeuvote.android.PollVote;


public class PollDetail extends Activity 
{
	
    private static Button btnMap; 
    private static Button btnVote; 
    private static Button btnBack;
    
    public void onCreate(Bundle icicle) 
    {
        super.onCreate(icicle);
        setContentView(R.layout.polldetail);
        
        String theStory = null;
        
        
        Intent startingIntent = getIntent();
        
        if (startingIntent != null)
        {
        	Bundle b = startingIntent.getBundleExtra("android.intent.extra.INTENT");
        	if (b == null)
        	{
        		theStory = "Sorry could not retrieve poll details";
        	}
        	else
    		{
        		theStory = b.getString("title") + "\n\n" + b.getString("pubdate") + "\n\n" + b.getString("description").replace('\n',' ') + "\n\nMore information:\n" + b.getString("link");
    		}
        }
        else
        {
        	theStory = "Information Not Found.";
        
        }
        
        TextView db= (TextView) findViewById(R.id.storybox);
        db.setText(theStory);
        
        btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(btnBackClick);
        btnMap = (Button) findViewById(R.id.btnMap);
        btnMap.setOnClickListener(btnMapClick);
        btnVote = (Button) findViewById(R.id.btnVote);
        btnVote.setOnClickListener(btnVoteClick);        
    }
 
    private final Button.OnClickListener btnBackClick = new Button.OnClickListener() 
    {
        public void onClick(View v) 
        {
        	finish();
        }
    };  
    
    private final Button.OnClickListener btnMapClick = new Button.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(PollDetail.this, PollMap.class);
            Bundle b = new Bundle();
            b.putInt("poll_id", (int) 1);
            i.putExtras(b);

            startActivity(i);
        }
    };  
    
    private final Button.OnClickListener btnVoteClick = new Button.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(PollDetail.this, PollVote.class);
            Bundle b = new Bundle();
            b.putInt("poll_id", (int) 1);
            i.putExtras(b);

            startActivity(i);
        }
    };       
    
    
}
