package com.placeuvote.android;

import java.io.File;
import java.io.FilenameFilter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.NotificationManager;
import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.placeuvote.android.rss.FeedData;
import com.placeuvote.android.rss.FeedDataContentProvider;
import com.placeuvote.android.rss.RefreshService;
import com.placeuvote.android.rss.Strings;

import com.placeuvote.android.R;

public class PollMain extends ListActivity {	
	
	private static final int MENU_REFRESH_ID = 2;
	
	private static final int CONTEXTMENU_EDIT_ID = 3;
	
	private static final int CONTEXTMENU_REFRESH_ID = 4;
	
	private static final int CONTEXTMENU_DELETE_ID = 5;
	
	private static final int CONTEXTMENU_MARKASREAD_ID = 6;
	
	private static final int MENU_SETTINGS_ID = 7;
	
	private static final int MENU_ALLREAD = 8;
	
	private static final int MENU_ABOUT_ID = 9;
	
	private static final int MENU_IMPORT_ID = 10;
	
	private static final int MENU_EXPORT_ID = 11;
	
	private static final int ACTIVITY_APPLICATIONPREFERENCES_ID = 1;
	
	
	static NotificationManager notificationManager; // package scope
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the feed
        this.AddFeeds();
        if (notificationManager == null) {
        	notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        setContentView(R.layout.main);
        setListAdapter(new PollMainList(this));
        getListView().setOnCreateContextMenuListener(new OnCreateContextMenuListener() {
			public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {
				menu.setHeaderTitle(((TextView) ((AdapterView.AdapterContextMenuInfo) menuInfo).targetView.findViewById(android.R.id.text1)).getText());
				menu.add(0, CONTEXTMENU_EDIT_ID, Menu.NONE, R.string.contextmenu_edit);
				menu.add(0, CONTEXTMENU_REFRESH_ID, Menu.NONE, R.string.contextmenu_refresh);
				menu.add(0, CONTEXTMENU_DELETE_ID, Menu.NONE, R.string.contextmenu_delete);
				menu.add(0, CONTEXTMENU_MARKASREAD_ID, Menu.NONE, R.string.contextmenu_markasread);
			}
        });
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Strings.SETTINGS_REFRESHENABLED, false)) {
        	startService(new Intent(this, RefreshService.class)); // starts the service independent to this activity
        } 
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Strings.SETTINGS_REFRESHONPENENABLED, false)) {
        	sendBroadcast(new Intent(Strings.ACTION_REFRESHFEEDS));
        }
        
        if (!FeedDataContentProvider.USE_SDCARD) {
        	Button button = (Button) findViewById(R.id.reload_button);
        	
        	button.setOnClickListener(new OnClickListener() {
    			public void onClick(View view) {
    				android.os.Process.killProcess(android.os.Process.myPid());
    			}
            });
        	button.setVisibility(View.VISIBLE);
        }
     }

	public void AddFeeds() {
		String id = "2";
		String url = "http://www.placeuvote.com/polls.rss";
		String name = "Public polls1";	
		Cursor cursor = getContentResolver().query(FeedData.FeedColumns.CONTENT_URI, new String[] {FeedData.FeedColumns._ID}, new StringBuilder(FeedData.FeedColumns.URL).append(Strings.DB_ARG).toString(), new String[] {url}, null);
	
		//if (cursor.moveToFirst()) {
		//	return;
		//} else {
			ContentValues values = new ContentValues();
			
			if (!url.startsWith(Strings.HTTP) && !url.startsWith(Strings.HTTPS)) {
				url = Strings.HTTP+url;
			}
			values.put(FeedData.FeedColumns.URL, url);
			values.put(FeedData.FeedColumns.FETCHMODE, 1);
			values.put(FeedData.FeedColumns.ERROR, (String) null);
	
			if (name.trim().length() > 0) {
				values.put(FeedData.FeedColumns.NAME, name);
			}
			getContentResolver().insert(FeedData.FeedColumns.CONTENT_URI, values);
		//}
		cursor.close();
	}


	@Override
	protected void onResume() {
		super.onResume();
		notificationManager.cancel(0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_REFRESH_ID, Menu.NONE, R.string.menu_refresh).setIcon(android.R.drawable.ic_menu_rotate);
		menu.add(0, MENU_SETTINGS_ID, Menu.NONE, R.string.menu_settings).setIcon(android.R.drawable.ic_menu_preferences);
		menu.add(0, MENU_ALLREAD, Menu.NONE, R.string.menu_allread).setIcon(android.R.drawable.ic_menu_revert);
		menu.add(0, MENU_ABOUT_ID, Menu.NONE, R.string.menu_about).setIcon(android.R.drawable.ic_menu_info_details);
		
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, final MenuItem item) {
		switch (item.getItemId()) {
			case MENU_REFRESH_ID: {
				sendBroadcast(new Intent(Strings.ACTION_REFRESHFEEDS));
				break;
			}
			case CONTEXTMENU_REFRESH_ID: {
				sendBroadcast(new Intent(Strings.ACTION_REFRESHFEEDS).putExtra(Strings.FEEDID, Long.toString(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id)));
				break;
			}

			case CONTEXTMENU_MARKASREAD_ID: {
				new Thread() {
					public void run() {
						getContentResolver().update(FeedData.EntryColumns.CONTENT_URI(Long.toString(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id)), getReadContentValues(), new StringBuilder(FeedData.EntryColumns.READDATE).append(Strings.DB_ISNULL).toString(), null);
					}
				}.start();
				break;
			}
			case MENU_SETTINGS_ID: {
				startActivityForResult(new Intent(this, ApplicationPreferencesActivity.class), ACTIVITY_APPLICATIONPREFERENCES_ID);
				break;
			}
			case MENU_ALLREAD: {
				new Thread() {
					public void run() {
						if (getContentResolver().update(FeedData.EntryColumns.CONTENT_URI, getReadContentValues(), new StringBuilder(FeedData.EntryColumns.READDATE).append(Strings.DB_ISNULL).toString(), null) > 0) {
							getContentResolver().notifyChange(FeedData.FeedColumns.CONTENT_URI, null);
						}
					}
				}.start();
				break;
			}
			case MENU_ABOUT_ID: {
				startActivity(new Intent(this, AboutActivity.class));
				break;
			}
		}
		return true;
	}
	
	public static final ContentValues getReadContentValues() {
		ContentValues values = new ContentValues();
		
		values.put(FeedData.EntryColumns.READDATE, System.currentTimeMillis());
		return values;
	}

	@Override
	protected void onListItemClick(ListView listView, View view, int position, long id) {
		Intent intent = new Intent(Intent.ACTION_VIEW, FeedData.EntryColumns.CONTENT_URI(Long.toString(id)));
		
		intent.putExtra(FeedData.FeedColumns.NAME, ((TextView) view.findViewById(android.R.id.text1)).getText());
		intent.putExtra(FeedData.FeedColumns.ICON, view.getTag() != null ? (byte[]) view.getTag() : null);
		startActivity(intent);
	}

	
	private Dialog createErrorDialog(int messageId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		builder.setMessage(messageId);
		builder.setTitle(R.string.error);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setPositiveButton(android.R.string.ok, null);
		return builder.create();
	}
	

    
}
