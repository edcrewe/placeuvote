/**
 * Sparse rss
 * 
 * Copyright (c) 2010 Stefan Handschuh
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.placeuvote.android.rss;

public final class Strings {
	public static final String PACKAGE = "com.placeuvote.android";
	
	public static final String SETTINGS_REFRESHINTERVAL = "refresh.interval";
	
	public static final String SETTINGS_NOTIFICATIONSENABLED = "notifications.enabled";
	
	public static final String SETTINGS_REFRESHENABLED = "refresh.enabled";
	
	public static final String SETTINGS_REFRESHONPENENABLED = "refreshonopen.enabled";
	
	public static final String SETTINGS_NOTIFICATIONSRINGTONE = "notifications.ringtone";
	
	public static final String SETTINGS_NOTIFICATIONSVIBRATE = "notifications.vibrate";
	
	public static final String SETTINGS_PRIORITIZE = "contentpresentation.prioritize";
	
	public static final String SETTINGS_SHOWTABS = "tabs.show";
	
	public static final String ACTION_REFRESHFEEDS = "com.placeuvote.android.rss.REFRESH";
	
	public static final String ACTION_UPDATEWIDGET = "com.placeuvote.android.rss.widget.UPDATE";
	
	public static final String ACTION_RESTART = "com.placeuvote.android.rss.RESTART";
	
	public static final String FEEDID = "feedid";
	
	public static final String DB_ISNULL = " IS NULL";
	
	public static final String DB_DESC = " DESC";
	
	public static final String DB_ARG = "=?";
	
	public static final String DB_AND = " AND ";
	
	public static final String EMPTY = "";

	public static final String SETTINGS_KEEPTIME = "keeptime";
	
	public static final String HTTP = "http://";
	
	public static final String HTTPS = "https://";

	public static final String PROTOCOL_SEPARATOR = "://";

	public static final String FILE_FAVICON = "/favicon.ico";
	
	public static final String SPACE = " ";
	
	public static final String TWOSPACE = "  ";

}
