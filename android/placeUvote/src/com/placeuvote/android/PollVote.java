package com.placeuvote.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.CheckBox;

import com.placeuvote.android.R;
import com.placeuvote.android.PUVDatabase.PollsCursor;


/**
 * PollVote
 */
public class PollVote extends Activity {
    private static Integer poll_id;
    private static TextView txtKey;
    private static CheckBox chkPrivate;
    private static CheckBox chkAnonymous;
    private static CheckBox chkOneChoice;
    private static TextView numLocations;
    private static TextView numDates;
    private static TextView dateCreatedOn;
    private static TextView txtCreatedBy;
    private static TextView dateLastVote;
    private static TextView numTotalVotes;
    private static TextView txtUser;
    private static TextView txtEmail;
    private static TextView txtQuestion;
    private static Button btnMap; 
    private static Button btnVote; 
    private static Button btnBack;
    
    PUVDatabase db;

    private PollsCursor poll;
    /**
    final CheckBox chkPrivate = (CheckBox) findViewById(R.id.chkPrivate);
    chkPrivate.setOnClickListener(new OnClickListener() {
	    public void onClick(View v) {
		// Perform action on clicks, depending on whether it's now checked
		if (((CheckBox) v).isChecked()) {
		    Toast.makeText(HelloFormStuff.this, "Selected", Toast.LENGTH_SHORT).show();
		} else {
		    Toast.makeText(HelloFormStuff.this, "Not selected", Toast.LENGTH_SHORT).show();
		}
	    }
	});
    */
	public boolean IntToBool(int intValue)
	{
	    return (intValue != 0);
	}
    
    /**
     * Called when the activity is first created.
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
/** Edit res/layout/polldetail.xml to match these form elements
 *  - txtEmail, txtUser, numLocations and numDates not displayed */
        setContentView(R.layout.pollvote);
        txtKey = (TextView) findViewById(R.id.txtKey);
        chkPrivate = (CheckBox) findViewById(R.id.chkPrivate);
        chkAnonymous = (CheckBox) findViewById(R.id.chkAnonymous);
        chkOneChoice = (CheckBox) findViewById(R.id.chkOneChoice);
        dateCreatedOn = (TextView) findViewById(R.id.dateCreatedOn);
        txtCreatedBy = (TextView) findViewById(R.id.txtCreatedBy);
        dateLastVote = (TextView) findViewById(R.id.dateLastVote);
        numTotalVotes = (TextView) findViewById(R.id.numTotalVotes);
        txtQuestion = (TextView) findViewById(R.id.txtQuestion);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnMap = (Button) findViewById(R.id.btnMap);


        // get the poll_id for this poll from the bundle passed by PollList
        Bundle bIn = this.getIntent().getExtras();
        //poll_id = Integer.valueOf(bIn.getInt("_id"));
        poll_id = 1;

        db = new PUVDatabase(this);
        poll = db.getPollDetails(poll_id.longValue());

		java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
	        // fill in the form and display
		txtKey.setText(poll.getColKey());
		chkPrivate.setChecked(IntToBool(poll.getColPrivate()));
		chkAnonymous.setChecked(IntToBool(poll.getColAnonymous()));
		chkOneChoice.setChecked(IntToBool(poll.getColOneChoice()));
		dateCreatedOn.setText("Created on: " + dateFormat.format(poll.getColCreatedOn()));
		txtCreatedBy.setText("Created by: " + poll.getColCreatedBy());
		dateLastVote.setText("Last vote: " + dateFormat.format(poll.getColLastVote()));
		numTotalVotes.setText("Total votes: " + Integer.toString(poll.getColTotalVotes()));
        // txtUser.setText(poll.getColUser());
        txtQuestion.setText(poll.getColQuestion());

        btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(btnBackClick);
        btnMap = (Button) findViewById(R.id.btnMap);
        btnMap.setOnClickListener(btnMapClick);
      
     }

    
    private final Button.OnClickListener btnBackClick = new Button.OnClickListener() 
    {
        public void onClick(View v) 
        {
        	finish();
        }
    };  
    
    private final Button.OnClickListener btnMapClick = new Button.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent(PollVote.this, PollMap.class);
            Bundle b = new Bundle();
            b.putInt("poll_id", (int) 1);
            i.putExtras(b);

            startActivity(i);
        }
    };  
    /**
     * Setup menus for this page.
     *
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean supRetVal = super.onCreateOptionsMenu(menu);
        menu.add(0, 0, Menu.NONE, getString(R.string.detail_menu_back_to_list));
        menu.add(0, 1, Menu.NONE, getString(R.string.detail_menu_poll_info));
        menu.add(0, 2, Menu.NONE, getString(R.string.detail_menu_delete_location));
        menu.add(0, 3, Menu.NONE, getString(R.string.detail_menu_edit_location));
        return supRetVal;
    }

    /**
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                // Go back to the list page
                finish();
                return true;
            case 1:
                // Go to the employer detail page
                Intent iEmp = new Intent(PollVote.this, VoterDetail.class);
                Bundle bEmp = new Bundle();
                bEmp.putInt("_id", poll_id.intValue());
                iEmp.putExtras(bEmp);

                startActivity(iEmp);
                return true;
            case 2:
            	// Delete this job
                // Setup Delete Alert Dialog
            	final int DELETE_JOB = 0;
            	final int CANCEL_DELETE = 1;
            	
                Handler mHandler = new Handler() {    	
                    public void handleMessage(Message msg) {
                        switch (msg.what) {
                            case DELETE_JOB:
                            db.deleteLocation(poll_id);
                            startActivity(new Intent(PollVote.this, PollList.class));
                            break;
                    
                            case CANCEL_DELETE:
                            // Do nothing
                            break;
                        }
                    }
                };
                // "Answer" callback.
                final Message acceptMsg = Message.obtain();
                acceptMsg.setTarget(mHandler);
                acceptMsg.what = DELETE_JOB;
                    
                // "Cancel" callback.
                final Message rejectMsg = Message.obtain();
                rejectMsg.setTarget(mHandler);
                rejectMsg.what = CANCEL_DELETE;

                new AlertDialog.Builder(this)
                  .setMessage("Are you sure you want to delete this poll?")
                  .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                	  public void onClick(DialogInterface dialog, int value) {
                		  rejectMsg.sendToTarget();
                	  }})
                  .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                	  public void onClick(DialogInterface dialog, int value) {
                    		  acceptMsg.sendToTarget();
                	  }})
                  .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                          rejectMsg.sendToTarget();
                      }})
                      .show();    
            	return true;
            case 3:
            	// Edit this job
            	// Start the Edit Location Activity, passing this poll's id
                Intent iEdit = new Intent(PollVote.this, EditPoll.class);
                Bundle bEdit = new Bundle();
                bEdit.putInt("_id", poll_id);
                iEdit.putExtras(bEdit);
                startActivity(iEdit);            	
            	return true;
            	
            default:
                return false;
        }
    }
    
}
