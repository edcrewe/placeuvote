PlaceUVote
==========

Google App Engine
-----------------

UPGRADE 0.96 to 1.2

Download latest appengine version.
cd google_appengine directory or gae1.*.* and symlink 

>>> ln -s /path/to/hg/clone/placeuvote/gaepuv placeuvote
>>> ./dev_appserver.py placeuvote

To check it runs on localhost:8080

To upgrade to newly supported django 1.2 must update main.py 

>> from google.appengine.dist import use_library
>> use_library('django', '1.2')

Fix main wrt. log exceptions work around 

Add |safe to html templates where needed, newforms to forms 
and forms self.cleaned_data to cleaned attribute
